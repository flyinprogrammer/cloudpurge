# cloudpurge

A simple CLI tool for purging cloud accounts. I use this tool to clear out accounts after interviews, teaching sessions, etc.

In theory some day soon it will support all kinds of cloud providers :D
