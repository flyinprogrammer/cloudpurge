// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/flyinprogrammer/cloudpurge/aws"
)

// accountIDCmd represents the accountID command
var accountIDCmd = &cobra.Command{
	Use:   "accountID",
	Short: "returns the account id of the account you're configured to use right now",
	Run: func(cmd *cobra.Command, args []string) {
		aws.SetupAWS(awsProfile)
		number, err := aws.GetAccountNumber()
		if err == nil {
			log.WithField("accountID", *number).Info("got account number")
		}
	},
}

func init() {
	awsCmd.AddCommand(accountIDCmd)
}
