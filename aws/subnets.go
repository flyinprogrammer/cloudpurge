package aws

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/hashicorp/terraform/dag"
	log "github.com/sirupsen/logrus"
)

func processSubnets(client *ClientWithRegionVPC, graph *dag.Graph) {
	subnets := getAllSubnets(client)
	for _, s := range subnets {
		sV := SubnetVertex{Subnet: s, Protected: isProtectedByTag(s.Tags)}
		graph.Add(sV)
		routeTable := getSubnetRouteTable(client, s.SubnetId)
		rV := RouteTableVertex{RouteTable: routeTable, Protected: isProtectedByTag(routeTable.Tags)}
		graph.Connect(dag.BasicEdge(rV, sV))
	}
}

func getAllSubnets(client *ClientWithRegionVPC) []*ec2.Subnet {
	describeSubnetsOutput, err := client.DescribeSubnets(&ec2.DescribeSubnetsInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name: aws.String("vpc-id"),
				Values: []*string{
					client.Vpc.VpcId,
				},
			},
		},
	})
	if err != nil {
		log.WithError(err).Error("error fetching subnets")
	}
	return describeSubnetsOutput.Subnets
}

func getSubnet(client *ClientWithRegionVPC, subnetId *string) (*ec2.Subnet, error) {
	describeSubnetsOutput, err := client.DescribeSubnets(&ec2.DescribeSubnetsInput{
		SubnetIds: []*string{subnetId},
	})
	if err != nil {
		log.WithError(err).WithField("subnet-id", aws.StringValue(subnetId)).Error("error getting specific subnet")
		return nil, err
	}
	if len(describeSubnetsOutput.Subnets) != 1 {
		log.WithError(err).WithField("subnet-id", aws.StringValue(subnetId)).Error("subnet response had too many subnets")
		return nil, err
	}
	return describeSubnetsOutput.Subnets[0], nil
}
