package aws

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/hashicorp/terraform/dag"
	log "github.com/sirupsen/logrus"
)

func processSecurityGroups(client *ClientWithRegionVPC, graph *dag.Graph) {
	securityGroups := getAllSecurityGroups(client)
	for _, s := range securityGroups {
		sV := SecurityGroupVertex{SecurityGroup: s, Protected: isProtectedByTag(s.Tags)}
		graph.Add(sV)
		graph.Connect(dag.BasicEdge(VPCVertex{Vpc: client.Vpc}, sV))
	}
}

func getAllSecurityGroups(client *ClientWithRegionVPC) []*ec2.SecurityGroup {
	describeSecurityGroups, err := client.DescribeSecurityGroups(&ec2.DescribeSecurityGroupsInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name: aws.String("vpc-id"),
				Values: []*string{
					client.Vpc.VpcId,
				},
			},
		},
	})
	if err != nil {
		log.WithError(err).Fatal("error fetching all security groups")
	}
	return describeSecurityGroups.SecurityGroups
}

func getSecurityGroup(client *ClientWithRegionVPC, securityGroupId *string) *ec2.SecurityGroup {
	describeSecurityGroups, err := client.DescribeSecurityGroups(&ec2.DescribeSecurityGroupsInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name: aws.String("vpc-id"),
				Values: []*string{
					client.Vpc.VpcId,
				},
			},
			&ec2.Filter{
				Name: aws.String("group-id"),
				Values: []*string{
					securityGroupId,
				},
			},
		},
	})
	if err != nil {
		log.WithError(err).Fatal("error fetching security group")
	}
	if len(describeSecurityGroups.SecurityGroups) != 1 {
		log.WithField("security groups", describeSecurityGroups.SecurityGroups).Fatal("security group count should have been 1")
	}
	return describeSecurityGroups.SecurityGroups[0]
}
