package aws

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/sts"
	log "github.com/sirupsen/logrus"
)

// EC2Client is our project wide client.
var EC2Client *ec2.EC2
var STSClient *sts.STS
var SESS *session.Session

type ClientWithRegionVPC struct {
	*ec2.EC2
	Region *ec2.Region
	Vpc    *ec2.Vpc
}

func SetupAWS(profile string) {
	log.WithField("profile", profile).Debug("using this profile")
	// Initial credentials loaded from SDK's default credential chain. Such as
	// the environment, shared credentials (~/.aws/credentials), or EC2 Instance
	// Role. These credentials will be used to to make the STS Assume Role API.
	SESS = session.Must(session.NewSessionWithOptions(session.Options{
		Profile:           profile,
		SharedConfigState: session.SharedConfigEnable,
	}))

	// Create service client value configured for credentials
	// from assumed role.
	EC2Client = ec2.New(SESS)
	STSClient = sts.New(SESS)

}

// GetAllRegions returns all the regions that are available for us to play with
func GetAllRegions() (regions []*ec2.Region, err error) {
	describeRegionsOutput, err := EC2Client.DescribeRegions(&ec2.DescribeRegionsInput{})
	if err != nil {
		log.WithError(err).Error("error describing regions")
		return
	}
	regions = describeRegionsOutput.Regions
	return
}

// GetAccountNumber get the account number with the current credentials
func GetAccountNumber() (accountID *string, err error) {
	getCallerIdentityOutput, err := STSClient.GetCallerIdentity(&sts.GetCallerIdentityInput{})
	if err != nil {
		log.WithError(err).Error("error getting caller identity")
		return
	}
	accountID = getCallerIdentityOutput.Account
	return
}

func BuildClientWithRegionVPCs(regions []*ec2.Region) []ClientWithRegionVPC {
	var clients []ClientWithRegionVPC

	for _, r := range regions {
		log.WithField("region", *r.RegionName).Debug("processing region")
		tmpEC2Client := ec2.New(SESS, &aws.Config{
			Region: r.RegionName,
		})

		vpcs, err := getAllVPCs(tmpEC2Client)
		if err != nil {
			log.WithError(err).Error("could not get all vpcs")
			continue
		}
		for _, v := range vpcs {
			clients = append(clients, ClientWithRegionVPC{
				EC2:    tmpEC2Client,
				Region: r,
				Vpc:    v,
			})
		}
	}
	return clients
}


// ProtectedTagKey if a resource has a Tag Key with the this value, then it is Protected.
const ProtectedTagKey = "exception"

func isProtectedByTag(tags []*ec2.Tag) bool {
	for _, t := range tags {
		if aws.StringValue(t.Key) == ProtectedTagKey {
			return true
		}
	}
	return false
}