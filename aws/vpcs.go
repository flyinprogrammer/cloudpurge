package aws

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/hashicorp/terraform/dag"
	log "github.com/sirupsen/logrus"
)

func processVPC(client *ClientWithRegionVPC, graph *dag.Graph) {
	graph.Add(VPCVertex{Vpc: client.Vpc})
	graph.Connect(dag.BasicEdge(
		RegionVertex{client.Region},
		VPCVertex{
			Vpc:       client.Vpc,
			Protected: isProtectedByTag(client.Vpc.Tags),
		},
	))

	// TODO: this is less wrong
	processRouteTables(client, graph)
	processSubnets(client, graph)
	processSecurityGroups(client, graph)
	processInstances(client, graph)
}

func getAllVPCs(ec2Client *ec2.EC2) (vpcs []*ec2.Vpc, err error) {
	var describeVpcsOutput *ec2.DescribeVpcsOutput
	describeVpcsOutput, err = ec2Client.DescribeVpcs(&ec2.DescribeVpcsInput{})
	if err != nil {
		log.WithError(err).Error("error fetching vpcs")
		return
	}
	vpcs = describeVpcsOutput.Vpcs
	return
}

func PurgeVPCs() {
	regions, err := GetAllRegions()
	if err != nil {
		log.WithError(err).Fatal("could not get all regions")
	}
	clients := BuildClientWithRegionVPCs(regions)
	for _, c := range clients {
		DeleteVPC(c.EC2, c.Vpc)
	}
}

func DeleteVPC(ec2Client *ec2.EC2, vpc *ec2.Vpc) (err error) {
	err = DeleteSubnets(ec2Client)
	if err != nil {
		return
	}
	err = DetatchIGWs(ec2Client)
	if err != nil {
		return
	}
	_, err = ec2Client.DeleteVpc(&ec2.DeleteVpcInput{
		VpcId: vpc.VpcId,
	})
	if err != nil {
		return
	}
	log.WithField("region", aws.StringValue(ec2Client.Config.Region)).WithField("vpc", aws.StringValue(vpc.VpcId)).Info("vpc deleted")
	return
}

func DetatchIGWs(ec2Client *ec2.EC2) (err error) {
	describeInternetGatewaysOutput, err := ec2Client.DescribeInternetGateways(&ec2.DescribeInternetGatewaysInput{})
	if err != nil {
		return
	}
	for _, i := range describeInternetGatewaysOutput.InternetGateways {
		for _, a := range i.Attachments {
			_, err = ec2Client.DetachInternetGateway(&ec2.DetachInternetGatewayInput{
				InternetGatewayId: i.InternetGatewayId,
				VpcId:             a.VpcId,
			})

			if err != nil {
				log.WithField("igw", *i.InternetGatewayId).WithField("vpc", *a.VpcId).WithError(err).Error("error detatching internet gateway from vpc")
				continue
			}

			log.WithField("igw", *i.InternetGatewayId).Info("detattached internet gateway")
		}
	}
	return
}

func DeleteSubnets(ec2Client *ec2.EC2) (err error) {
	describeSubnetsOutput, err := ec2Client.DescribeSubnets(&ec2.DescribeSubnetsInput{})
	if err != nil {
		return
	}
	for _, s := range describeSubnetsOutput.Subnets {
		_, err := ec2Client.DeleteSubnet(&ec2.DeleteSubnetInput{
			SubnetId: s.SubnetId,
		})
		if err != nil {
			log.WithField("subnet", *s.SubnetId).WithError(err).Error("error deleting subnet")
			continue
		}
		log.WithField("subnet", *s.SubnetId).Info("deleting subnet")
	}
	return
}
