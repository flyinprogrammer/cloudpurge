package aws

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/hashicorp/terraform/dag"
	log "github.com/sirupsen/logrus"
)

func processRouteTables(client *ClientWithRegionVPC, graph *dag.Graph) {
	routeTables := getAllRouteTables(client)
	for _, r := range routeTables {
		rV := RouteTableVertex{RouteTable: r, Protected: isProtectedByTag(r.Tags)}
		graph.Add(rV)
		graph.Connect(dag.BasicEdge(VPCVertex{Vpc: client.Vpc}, rV))
	}
}

func getAllRouteTables(client *ClientWithRegionVPC) []*ec2.RouteTable {
	describeRouteTablesOutput, err := client.DescribeRouteTables(&ec2.DescribeRouteTablesInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name: aws.String("vpc-id"),
				Values: []*string{
					client.Vpc.VpcId,
				},
			},
		},
	})
	if err != nil {
		log.WithError(err).Fatal("error fetching all routetables")
	}
	return describeRouteTablesOutput.RouteTables
}

func getMainRouteTable(client *ClientWithRegionVPC) *ec2.RouteTable {
	describeRouteTablesOutput, err := client.DescribeRouteTables(&ec2.DescribeRouteTablesInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name: aws.String("vpc-id"),
				Values: []*string{
					client.Vpc.VpcId,
				},
			},
			&ec2.Filter{
				Name:   aws.String("association.main"),
				Values: []*string{aws.String("true")},
			},
		},
	})
	if err != nil {
		log.WithError(err).Fatal("error fetching all routetables")
	}
	if len(describeRouteTablesOutput.RouteTables) != 1 {
		log.WithField("route tables", describeRouteTablesOutput.RouteTables).Fatal("there should only be 1 main route table per vpc")
	}
	return describeRouteTablesOutput.RouteTables[0]
}

func getSubnetRouteTable(client *ClientWithRegionVPC, subnetId *string) *ec2.RouteTable {
	describeRouteTablesOutput, err := client.DescribeRouteTables(&ec2.DescribeRouteTablesInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name: aws.String("vpc-id"),
				Values: []*string{
					client.Vpc.VpcId,
				},
			},
			&ec2.Filter{
				Name:   aws.String("association.subnet-id"),
				Values: []*string{subnetId},
			},
		},
	})
	if err != nil {
		log.WithError(err).Fatal("error fetching all routetables")
	}
	if len(describeRouteTablesOutput.RouteTables) == 0 {
		return getMainRouteTable(client)
	}
	if len(describeRouteTablesOutput.RouteTables) > 1 {
		log.WithField("route tables", describeRouteTablesOutput.RouteTables).Fatal("more than one route table associated with a subnet")
	}
	return describeRouteTablesOutput.RouteTables[0]
}
