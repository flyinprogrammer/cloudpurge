package aws

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/hashicorp/terraform/dag"
	log "github.com/sirupsen/logrus"
)

func processInstances(client *ClientWithRegionVPC, graph *dag.Graph) {
	instances := getAllInstance(client)
	for _, i := range instances {
		iV := InstanceVertex{Instance: i, Protected: isProtectedByTag(i.Tags)}
		graph.Add(iV)
		subnet, err := getSubnet(client, i.SubnetId)
		if err != nil {
			log.WithError(err).Error("cannot add subnet -> instance association")
		} else {
			graph.Connect(dag.BasicEdge(
				SubnetVertex{Subnet: subnet, Protected: isProtectedByTag(subnet.Tags)},
				iV,
			))
		}
		for _, sg := range i.SecurityGroups {
			securityGroup := getSecurityGroup(client, sg.GroupId)
			sV := SecurityGroupVertex{SecurityGroup: securityGroup, Protected: isProtectedByTag(securityGroup.Tags)}
			graph.Connect(dag.BasicEdge(iV, sV))
		}
	}
}

func getAllInstance(client *ClientWithRegionVPC) (instances []*ec2.Instance) {
	var nextToken *string
	for {
		log.WithField("vpc-id", aws.StringValue(client.Vpc.VpcId)).Debug("working on this vpc")
		describeInstancesOutput, err := client.DescribeInstances(&ec2.DescribeInstancesInput{
			NextToken: nextToken,
			Filters: []*ec2.Filter{
				&ec2.Filter{
					Name: aws.String("vpc-id"),
					Values: []*string{
						client.Vpc.VpcId,
					},
				},
			},
		})
		if err != nil {
			log.WithError(err).Error("could not describe instances")
			break
		}
		log.WithField("reservation", describeInstancesOutput.Reservations).Debug("found these reservations")
		for _, r := range describeInstancesOutput.Reservations {
			log.WithField("instances", r.Instances).Debug("found these instances")
			instances = append(instances, r.Instances...)
		}
		log.WithField("nextToken", nextToken).Debug("next token found")
		if describeInstancesOutput.NextToken != nil {
			nextToken = describeInstancesOutput.NextToken
		} else {
			break
		}
	}
	return
}
