package aws

import (
	"fmt"

	"github.com/hashicorp/terraform/dag"
	log "github.com/sirupsen/logrus"
)

func DrawGraph() {
	// TODO: Draw the graph.
	regions, err := GetAllRegions()
	if err != nil {
		log.WithError(err).Fatal("could not get all regions while trying to draw graph")
	}
	var graph dag.Graph
	for _, r := range regions {
		graph.Add(RegionVertex{r})
	}

	clients := BuildClientWithRegionVPCs(regions)

	for _, c := range clients {
		processVPC(&c, &graph)
	}

	// -----------------

	fmt.Println(string(graph.Dot(nil)))
}
