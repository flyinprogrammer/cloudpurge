package aws

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/directconnect"
	"github.com/aws/aws-sdk-go/service/ec2"
)

type RegionVertex struct {
	*ec2.Region
}

func (rV RegionVertex) Name() string {
	return aws.StringValue(rV.RegionName)
}

type VPCVertex struct {
	*ec2.Vpc
	Protected bool
}

func (vV VPCVertex) Name() string {
	return fmt.Sprintf("%s - %t", aws.StringValue(vV.VpcId), vV.Protected)
}

type SubnetVertex struct {
	*ec2.Subnet
	Protected bool
}

func (sV SubnetVertex) Name() string {
	return fmt.Sprintf("%s - %t", aws.StringValue(sV.SubnetId), sV.Protected)
}

type RouteTableVertex struct {
	*ec2.RouteTable
	Protected bool
}

func (rV RouteTableVertex) Name() string {
	return fmt.Sprintf("%s - %t", aws.StringValue(rV.RouteTableId), rV.Protected)
}

type IGWVertex struct {
	*ec2.InternetGateway
	Protected bool
}

func (iV IGWVertex) Name() string {
	return fmt.Sprintf("%s - %t", aws.StringValue(iV.InternetGatewayId), iV.Protected)
}

type EgressOnlyInternetGatewayVertex struct {
	*ec2.EgressOnlyInternetGateway
	Protected bool
}

func (eV EgressOnlyInternetGatewayVertex) Name() string {
	return fmt.Sprintf("%s - %t", aws.StringValue(eV.EgressOnlyInternetGatewayId), eV.Protected)
}

type DhcpOptionsVertex struct {
	*ec2.DhcpOptions
	Protected bool
}

func (dV DhcpOptionsVertex) Name() string {
	return fmt.Sprintf("%s - %t", aws.StringValue(dV.DhcpOptionsId), dV.Protected)
}

type AddressVertex struct {
	*ec2.Address
	Protected bool
}

func (aV AddressVertex) Name() string {
	return fmt.Sprintf("%s - %t", aws.StringValue(aV.PublicIp), aV.Protected)
}

type VpcEndpointVertex struct {
	*ec2.VpcEndpoint
	Protected bool
}

func (eV VpcEndpointVertex) Name() string {
	return fmt.Sprintf("%s - %t", aws.StringValue(eV.VpcEndpointId), eV.Protected)
}

type NatGatewayVertex struct {
	*ec2.NatGateway
	Protected bool
}

func (nV NatGatewayVertex) Name() string {
	return fmt.Sprintf("%s - %t", aws.StringValue(nV.NatGatewayId), nV.Protected)
}

type VpcPeeringConnectionVertex struct {
	*ec2.VpcPeeringConnection
	Protected bool
}

func (pV VpcPeeringConnectionVertex) Name() string {
	return fmt.Sprintf("%s - %t", aws.StringValue(pV.VpcPeeringConnectionId), pV.Protected)
}

type NetworkAclVertex struct {
	*ec2.NetworkAcl
	Protected bool
}

func (nV NetworkAclVertex) Name() string {
	return fmt.Sprintf("%s - %t", aws.StringValue(nV.NetworkAclId), nV.Protected)
}

type SecurityGroupVertex struct {
	*ec2.SecurityGroup
	Protected bool
}

func (sV SecurityGroupVertex) Name() string {
	return fmt.Sprintf("%s - %t", aws.StringValue(sV.GroupId), sV.Protected)
}

type CustomerGatewayVertex struct {
	*ec2.CustomerGateway
	Protected bool
}

func (cV CustomerGatewayVertex) Name() string {
	return fmt.Sprintf("%s - %t", aws.StringValue(cV.CustomerGatewayId), cV.Protected)
}

type VirtualGatewayVertex struct {
	*directconnect.VirtualGateway
	Protected bool
}

func (vV VirtualGatewayVertex) Name() string {
	return fmt.Sprintf("%s - %t", aws.StringValue(vV.VirtualGatewayId), vV.Protected)
}

// ---
type InstanceVertex struct {
	*ec2.Instance
	Protected bool
}

func (iV InstanceVertex) Name() string {
	return fmt.Sprintf("%s - %t", aws.StringValue(iV.InstanceId), iV.Protected)
}
